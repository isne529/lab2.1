#include <iostream>
#include "list.h"
using namespace std;

void main()
{
	//Sample Code
	List mylist;
	mylist.pushToHead('k');
	mylist.pushToHead('e');
	mylist.pushToHead('n');
	cout << "Current : ";
	mylist.print();
	cout << endl;
	mylist.reverse();
	cout << "Reverse : ";
	mylist.print();//
	cout << endl;
	mylist.popTail();
	cout << "After PopTail : ";
	mylist.print();
	cout << endl;
	char input;//create char value for search
	cout << "Input a character for Search : ";
	cin >> input;//recieve character
	if (mylist.search(input) == true) {//if call function is true display character found
		cout << "Character found!\n";
	}
	else {//if call function is false display character didn't found
		cout << "Character didn't found!\n";
	}
	system("pause");


}